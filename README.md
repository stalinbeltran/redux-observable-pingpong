## Short redux-observable example source code
While checking redux-observable I found there were few sample codes. There were short source codes who doesn't worked. There were larger codes that worked but were hard to understand.

So I decided to make my own, short and working, as simple as possible. This, of course, does not replace a good tutorial, but is useful for those of you who, like me, learn by doing.


![ObservablePingPong screenshot](public/ObservablePingPong.png)

---
## Expected behavior
When you click the button, you are making a "PING".
After a couple of seconds, a "PONG" is automatically produced (by virtue of redux-observable async code).
When checking the console, you will see the redux actions as they happen.
	
---
## How install

1. In your computer, install Node (which comes with npm, what we need)
2. Clone this repository (or download it) to a folder on your computer
3. cd to that folder
4. Install the downloaded code. In your console (cmd for windows) type:

	**npm install**
	
5. Start the code:

	**npm start**
	
	Your browser must open, showing this app in all its glory :)
	
6. Start tinkering with the code.

---


