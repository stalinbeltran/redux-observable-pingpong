/*
Code based on
https://jsbin.com/jexomi/edit?js,output
modified to work on create-react-app
Several files where removed, so the code looks simpler

*/

import React from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux'
import {createStore, applyMiddleware } from 'redux'
import {createEpicMiddleware} from 'redux-observable'
import { filter, mapTo, delay } from 'rxjs/operators';

console.clear();

//declare a couple of constants, so we don't have typing errors
const PING = 'PING';
const PONG = 'PONG';

const clickOnStartPingButton = function (){
  console.log('click on "Start PING" button')
  return { type: PING }
}


/*pingReducer changes the state of the app, given both a state and an action
Please note the state here is really simple: { isPinging: true/false }
this is called by the store once an action is dispatched
*/
const pingReducer = (state = { isPinging: false }, action) => {
  console.log(action)
  switch (action.type) {
    case PING:
      return { isPinging: true };

    case PONG:
      return { isPinging: false };

    default:
      return state;
  }
};


/*
pingEpic defines additional actions to be done based on the input actions.
action$ (with a trailing '$') is just a naming convention for 'stream actions'
(the ones redux-observable is useful for).
Here we filter actions of type PING, then wait 2000 ms (this could simulate an asynchronous action),
and them we change the action returned to type PONG. In short, we generate a PONG for every PING.
*/
const pingEpic = action$ => action$.pipe(   //chain several rxjs operators
  filter(action => action.type === PING),   //filter PING actions only
  delay(2000),  //wait 2000 ms. Needed to see both states. Otherwise, it you will see no change in the browser
  mapTo({ type: PONG })     //maps each action to a new action, PONG in this case
);




let AppComponent = ({ isPinging, clickHandler }) => (
  <div>
    <h1>is pinging: {isPinging.toString()}</h1>
    <button onClick={clickHandler}>Start PING</button>
  </div>
);

const mapStateToProps = function ({ isPinging }){
  return {isPinging}
}

/*
Please note the AppComponent button require a property named clickHandler to be able
to handle the click event, so we have to provide that property passing {clickHandler : clickOnStartPingButton}
*/
const App = connect(
  mapStateToProps, {clickHandler : clickOnStartPingButton}    //we define the clickHandler prop as our function clickOnStartPingButton
                                                      //(required by button in AppComponent)
)(AppComponent);           //here we wrap our component for it to be connected to the store

const epicMiddleware = createEpicMiddleware();      //redux-observable middleware
const store = createStore(pingReducer, applyMiddleware(epicMiddleware));    //apply it to our store
epicMiddleware.run(pingEpic)    //Run the middleware with the provided epic function

/*Provider is a wraper for all our app, to make the store available*/
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
